#   Import data processing packages
import json
import pandas as pd
import joblib

#   Import natural language took kits
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

#   Other imports
from flask import Flask
from flask import render_template, request, jsonify
import plotly
from plotly.graph_objs import Bar, Scatter
from sqlalchemy import create_engine

app = Flask(__name__)

def tokenize(text):
    #   convert each text input into tokens
    tokens = word_tokenize(text)

    #   initialize lemmatizer for converting tokens to root
    lemmatizer = WordNetLemmatizer()

    clean_tokens = []
    for tok in tokens:
        clean_tok = lemmatizer.lemmatize(tok).lower().strip()
        clean_tokens.append(clean_tok)

    #   remove stopwords    
    clean_tokens = [x for x in clean_tokens if x not in stopwords.words('english')]

    return clean_tokens

#   load data
engine = create_engine('sqlite:///data/DisasterResponse.db')
df = pd.read_sql_table('DisasterResponse', engine)
metrics = pd.read_csv("data/model_metrics.csv")

#   load model
model = joblib.load("models/classifier.pkl")

if engine is None :
    print('Engine is Empty')

if df is None :
    print('SQL Table is Empty')

if model is None :
    print('Model is Empty')

#   index webpage receives user input text for model
@app.route('/')
@app.route('/index')
def index():
    
    #   Figure 1: data showing Number of Negative-Positive class per category - top 5
    genre_per_category = df.iloc[:,3:].groupby('genre').sum().T
    top_category = genre_per_category.sum(axis=1).sort_values(ascending=False).reset_index()
    top_category.columns = ['categories', 'true_proportion -1']
    top_category['false_proportion -0'] = df.shape[0] - top_category['true_proportion -1']
    top_category['categories'] = top_category['categories'].apply(lambda x: str(x).replace('_', ' '))
    top_classes = top_category.head(5)

    #   Figure 3: data visuals
    genre_counts = df.groupby('genre').count()['message']
    genre_names = list(genre_counts.index)

    #   Create visuals
    graphs = [
        {
            'data': [
                Bar(
                    name = 'Positive',
                    y=top_classes['categories'],
                    x=top_classes['true_proportion -1'],
                    orientation = 'h',
                    marker=dict(
                            color='rgba(246, 78, 139, 0.6)',
                            line=dict(color='rgba(246, 78, 139, 1.0)', width=3))
                    
                ),
                Bar(
                    name = 'Negative',
                    y=top_classes['categories'],
                    x=top_classes['false_proportion -0'],
                    orientation = 'h',
                    marker=dict(
                            color='rgba(58, 71, 80, 0.6)',
                            line=dict(color='rgba(58, 71, 80, 1.0)', width=3)
    )
                )
            ],
            'layout':{
                'barmode' : 'stack',
                'title': 'Number of Negative-Positive class per category (top 5)',
                "xaxis": {
                    'title': 'Number of messages'
                },
                "yaxis": {
                    'title': 'Categories',
                    'title_standoff' : 40, 
                    'tickangle' : 45
                },
            }

        },
        {
            'data':[
                Scatter(
                    name = 'Precision',
                    x = metrics['Target Category'],
                    y = metrics['PRECISION'],
                    mode = 'lines'
                ),
                Scatter(
                    name = 'Recall',
                    x = metrics['Target Category'],
                    y = metrics['RECALL'],
                    mode = 'lines'
                ),
                Scatter(
                    name = 'F1 Score',
                    x = metrics['Target Category'],
                    y = metrics['F1'],
                    mode = 'lines'
                )
            ],
            'layout':{
                'title': 'AdaBoost Model Performance Metrics',
                "xaxis":{
                    'title': 'Categories',
                    'title_standoff': 100,
                    'tickangle': 45
                },
                "yaxis":{
                     'title': ""
                }
            }

        },
       {
            'data': [
                Bar(
                    x=genre_names,
                    y=genre_counts,
                    marker=dict(
                            color='rgba(174, 132, 255, 0.99)',
                            line=dict(color='rgba(174, 132, 255, 0.99)', width=3))
                )
            ],
            'layout': {
                'title': 'Distribution of Message Genres',
                'yaxis': {
                    'title': "Count"
                },
                'xaxis': {
                    'title': "Genre"
                },
                'template': "seaborn"
            }
        },
    ]
    
    #   encode plotly graphs in JSON
    ids = ["graph-{}".format(i) for i, _ in enumerate(graphs)]
    graphJSON = json.dumps(graphs, cls=plotly.utils.PlotlyJSONEncoder)
    
    #   render web page with plotly graphs
    return render_template('index.html', ids=ids, graphJSON=graphJSON)


#   web page that handles user query and displays model results
@app.route('/go')
def go():
    
    # save user input in query
    query = request.args.get('query', '') 

    # use model to predict classification for query
    classification_labels = model.predict([query])[0]
    classification_results = dict(zip(df.columns[4:], classification_labels))

    # This will render the go.html Please see that file. 
    return render_template(
        'results.html',
        query=query,
        classification_result=classification_results)


def main():
    app.run(host='0.0.0.0', port=3001, debug=True)
 

if __name__ == '__main__':
    main()